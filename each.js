const each = (array, cb) => {
  if (Array.isArray(array)) {
    for (let index = 0; index < array.length; index++) {
      cb(array[index], index, array);
    }
  }
};

module.exports = each;
