const each = require("./each");

const filter = (array, cb) => {
  if (Array.isArray(array)) {
    let filteredArray = [];
    each(array, (element, index, array) => {
      if (cb(element, index, array) === true) {
        filteredArray.push(element);
      }
    });
    return filteredArray;
  }
};

module.exports = filter;
