const filter = require("../filter");
const expect = require("chai").expect;
const items = require("../arrays");

describe("Test Filter module", () => {
  it("should return filtered array", function () {
    let result = filter(items, (element) => {
      return element > 3;
    });
    expect(result).to.deep.equal([4, 5, 5]);
  });
  it("should return undefined when arguments are not valid", function () {
    let result = filter("123");
    expect(result).to.equal(undefined);
  });
});
