const reduce = require("../reduce");
const expect = require("chai").expect;
const items = require("../arrays");

describe("Test Reduce module", () => {
  it("should return reduced array", function () {
    const result = reduce(
      items,
      (previousValue, currentValue) => {
        return previousValue + currentValue;
      },
      0
    );
    expect(result).to.equal(20);
  });
  it("should return reduced array if initialValue is not provided", function () {
    const result = reduce(items, (previousValue, currentValue) => {
      return previousValue + currentValue;
    });
    expect(result).to.equal(20);
  });
  it("should return undefined if invalid arguments are provided", function () {
    const result = reduce("hello", (previousValue, currentValue) => {
      expect(result).to.equal(undefined);
    });
  });
});
