const flat = require("../flatten");
const expect = require("chai").expect;
const items = [1, [2], 3, , [4, null, [5, [6, [7, 8, [9, 10]]]]]];

describe("Test Flat module", () => {
  it("should return array as it is provided when depth is 0", function () {
    let result = flat(items, 0);
    expect(result).to.deep.equal(items.flat(0));
  });
  it("should return array flatten with depth 1 when depth is not provided", function () {
    let result = flat(items);
    expect(result).to.deep.equal(items.flat());
  });
  it("should flatten array to infinite depth", function () {
    let result = flat(items, Infinity);
    expect(result).to.deep.equal(items.flat(Infinity));
  });
  it("should flatten array to specified depth", function () {
    let result = flat(items, 2);
    expect(result).to.deep.equal(items.flat(2));
  });
  it("should return provided argument as it is if argument is not a valid array", function () {
    let result = flat("123");
    expect(result).to.equal("123");
  });
});
