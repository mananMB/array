const each = require("../each.js");
const expect = require("chai").expect;
const items = require("../arrays");

describe("Test each module", () => {
  it("should perform operation on each value of the array", () => {
    let result = [];
    let expectedResult = [];

    const cb = (element, index, array) => {
      return result.push([element, index, array]);
    };

    items.forEach((element, index, array) => {
      return expectedResult.push([element, index, array]);
    });
    each(items, cb);
    expect(result).deep.equal(expectedResult);
  });
  it("should return undefined when valid arguments are not provided", function () {
    expect(each()).to.equal(undefined);
  });
});
