const map = require("../map");
const expect = require("chai").expect;
const items = require("../arrays");

describe("Test Map module", () => {
  it("should should return mapped array with callback function performed on it", function () {
    const cb = (element, index, array) => {
      return element * 2;
    };
    let result = map(items, cb);
    expect(result).to.deep.equal([2, 4, 6, 8, 10, 10]);
  });
  it("should return undefined if argument is not valid", function () {
    let result = map("123");
    expect(result).to.equal(undefined);
  });
});
