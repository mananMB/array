const find = require("../find");
const expect = require("chai").expect;
const items = require("../arrays");

describe("Test Find module", () => {
  it("should should return the element if found", function () {
    let result = find(items, (element) => {
      return element > 3;
    });
    expect(result).to.equal(4);
  });
  it("should return undefined if element is not found", function () {
    let result = find(items, (element) => {
      return element > 10;
    });
    expect(result).to.equal(undefined);
  });
  it("should return undefined when arguments are not valid", function () {
    let result = find("123");
    expect(result).to.equal(undefined);
  });
});
