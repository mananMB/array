const each = require("./each");

const map = (array, cb) => {
  if (Array.isArray(array)) {
    let processedArray = [];
    each(array, (element, index, array) => {
      processedArray.push(cb(array[index], index, array));
    });
    return processedArray;
  }
};

module.exports = map;
