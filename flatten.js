const reduce = require("./reduce");
const filter = require("./filter");

function flatten(array, depth = 1) {
  if (!Array.isArray(array)) {
    return array;
  }
  array = filter(array, (element) => {
    return element !== undefined;
  });

  if (depth < 1) {
    return array;
  }

  return reduce(
    array,
    (initialValue, currentValue) => {
      return initialValue.concat(flatten(currentValue, depth - 1));
    },
    []
  );
}

const items = [1, [2], 3, , [4, null, [5, [6, [7, 8, [9, 10]]]]]];

flatten(items);

module.exports = flatten;
