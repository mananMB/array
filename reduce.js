const reduce = (array, cb, initialValue) => {
  let initialIndex = 0;

  if (initialValue === undefined) {
    initialValue = array[0];
    initialIndex = 1;
  } //if initialValue is not provided, first index of array becomes initialValue and the initialIndex increments to 1
  if (Array.isArray(array)) {
    for (let index = initialIndex; index < array.length; index++) {
      initialValue = cb(initialValue, array[index], index, array);
    }

    return initialValue;
  }
};

module.exports = reduce;
