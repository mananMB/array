const find = (array, cb) => {
    if(Array.isArray(array)){
        for (let index = 0; index < array.length; index++) {
            if(cb(array[index], index, array)){
                return array[index];
            }
        }
    }
}

module.exports = find